console.log('Starting directory: ' + process.cwd());
try {
  // process.chdir('..');
  console.log('New directory: ' + process.cwd());
}
catch (err) {
  console.log('chdir: ' + err);
}
const user = require('./db/users');
const messages = require('./db/messages')();
const tasks = require('./db/tasks')();
const jsonServer = require('json-server');
const faker = require('faker');
const server = jsonServer.create();
const router = jsonServer.router({
  user: user(),
  messages: messages,
  tasks: tasks
});
const path = require('path');
const middlewares = jsonServer.defaults({
  static: path.join(process.cwd(), 'dist')
});

server.get('/user', (req, res) => {
  res.json(user());
});

server.use(jsonServer.bodyParser);
server.use((req, res, next) => {
  if (req.originalUrl.startsWith('/tasks')) {
    if (req.method === 'PUT' && req.body.status === 'done') {
      req.body.completedAt = Date.now()
    } else if (req.method === 'POST') {
      Object.assign(req.body, {
        assignee: faker.name.findName(),
        email: faker.internet.email(),
        createdAt: new Date(),
        status: "toDo",
        size: faker.random.number()
      })
    }
  }
  next();
});

server.use(middlewares);
server.use(router);
server.listen(3000, () => {
  console.log('JSON Server is running at localhost:3000')
});

