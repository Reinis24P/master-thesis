const faker = require('faker');

module.exports = () => {
  const card = faker.helpers.createCard();
  delete card.posts;
  delete card.accountHistory;
  delete card.address;

  return Object.assign({}, card, {
    password: faker.internet.password(),
    roles: faker.random.arrayElement([["admin", "client", "user"], ["user"], ["admin", "user"], ["admin"]])
  });
};
