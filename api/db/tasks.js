const faker = require('faker');

module.exports = () => {
  const data = [];
  const taskAmount = 100;

  for (let i = 0; i < taskAmount; i++) {
    const type = faker.random.arrayElement(["bugs", "website", "server"]);
    let status = faker.random.arrayElement(["toDo", "inProgress", "testing", "done"]);
    let completedAt;

    if (status === 'done' || faker.random.boolean()) {
      completedAt = faker.date.recent();
      status = 'done';
    }

    data[i] = Object.assign({}, {
      id: i + 1,
      assignee: faker.name.findName(),
      email: faker.internet.email(),
      createdAt: faker.date.past(),
      name: faker.lorem.sentence(),
      type: type,
      status: status,
      completedAt: completedAt,
      size: faker.random.number()
    })
  }


  return data;
};
