const faker = require('faker');

module.exports = () => {
  const data = [];
  const messageAmount = 200;

  for (let i = 0; i < messageAmount; i++) {
    data[i] = Object.assign({}, {
      id: i + 1,
      from: faker.helpers.userCard(),
      received: faker.date.past(),
      message: faker.lorem.sentences(),
      read: faker.random.boolean(),
      size: faker.random.number()
    })
  }

  return data;
};
