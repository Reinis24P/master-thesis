import {Component} from "@angular/core";
import {Task, TaskType} from "../../../http/tasks/task.model";
import {BaseTask} from "../base-tasks";

@Component({
  selector: "app-bugs",
  templateUrl: "./bugs.component.html",
  styleUrls: ["./bugs.component.scss"],
})
export class BugsComponent extends BaseTask {
  public type: TaskType = "bugs";

  public filterTask(task: Task): boolean {
    return task.isBugType();
  }
}
