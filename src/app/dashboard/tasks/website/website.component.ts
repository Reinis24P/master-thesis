import {Component} from "@angular/core";
import {Task, TaskType} from "../../../http/tasks/task.model";
import {BaseTask} from "../base-tasks";

@Component({
  selector: "app-website",
  templateUrl: "./website.component.html",
  styleUrls: ["./website.component.scss"],
})
export class WebsiteComponent extends BaseTask {
  public type: TaskType = "website";

  public filterTask(task: Task): boolean {
    return task.isWebsiteType();
  }
}
