import {Component, Input, OnInit} from "@angular/core";
import * as Chartist from "chartist";
import {BaseLineChart} from "../../../shared/components/chart/base-line-chart";
import {Task} from "../../../http/tasks/task.model";

@Component({
  selector: "app-completed-tasks-chart",
  templateUrl: "./completed-tasks-chart.component.html",
  styleUrls: ["./completed-tasks-chart.component.scss"],
})
export class CompletedTasksChartComponent extends BaseLineChart implements OnInit {
  @Input()
  public tasks: Task[] = [];

  ngOnInit() {
    const dataCompletedTasksChart: any = {
      labels: this.getLabels().map((label) => label.getHours()),
      series: [
        this.getSeries()
      ]
    };

    const max = this.getMax();
    const optionsCompletedTasksChart = {
      lineSmooth: Chartist.Interpolation.cardinal({
        tension: 0
      }),
      low: 0,
      high: max + (max / 4),
      chartPadding: {top: 0, right: 0, bottom: 0, left: 0}
    };

    const completedTasksChart = new Chartist.Line("#completedTasksChart", dataCompletedTasksChart, optionsCompletedTasksChart);

    this.startAnimationForLineChart(completedTasksChart);
  }

  public calculate(): void {
    this.tasks = this.tasks.filter((task) => task.isCompleted());
    this.ngOnInit();
  }

  private getSeries(): number[] {
    return this.getLabels().map((label) => this.getAmountFromHour(label));
  }

  private getMax(): number {
    return Math.max(...this.getSeries());
  }

  private getOldestDate(): Date {
    const all_dates = this.tasks.map((task) => task.completedAt);
    let max_dt = all_dates[0];
    let max_dtObj = new Date(all_dates[0]);

    all_dates.forEach((dt) => {
      if (new Date(dt) < max_dtObj) {
        max_dt = dt;
        max_dtObj = new Date(dt);
      }
    });

    return max_dt;
  }

  private getLatestDate(): Date {
    const all_dates = this.tasks.map((task) => task.completedAt);
    let max_dt = all_dates[0];
    let max_dtObj = new Date(all_dates[0]);

    all_dates.forEach((dt) => {
      if (new Date(dt) > max_dtObj) {
        max_dt = dt;
        max_dtObj = new Date(dt);
      }
    });

    return max_dt;
  }

  private getLabels(): Date[] {
    let result = [];
    const oldestDate = this.getOldestDate();

    if (oldestDate) {
      const copiedOldestDate = new Date(oldestDate.getTime());
      const period = Math.ceil(Math.abs(this.getLatestDate().getTime() - oldestDate.getTime()) / 36e5);
      for (let index = 0; index < period; index++) {
        copiedOldestDate.setHours(copiedOldestDate.getHours() + 1);
        result[index] = new Date(copiedOldestDate);
      }
    }
    return result;
  }

  private getAmountFromHour(date: Date): number {
    const dateCopy = new Date(date);
    dateCopy.setMinutes(0);
    dateCopy.setSeconds(0);
    dateCopy.setMilliseconds(0);

    return this.tasks.filter((task) => {
      return task.completedAt < date;
    }).length;
  }
}
