import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {CompletedTasksChartComponent} from "./completed-tasks-chart.component";

describe("CompletedTasksChartComponent", () => {
  let component: CompletedTasksChartComponent;
  let fixture: ComponentFixture<CompletedTasksChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompletedTasksChartComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedTasksChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
