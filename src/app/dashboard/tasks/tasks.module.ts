import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {TasksComponent} from "./tasks.component";
import {FixedTasksSummaryComponent} from "./fixed-tasks-summary/fixed-tasks-summary.component";
import {OpenTasksSummaryComponent} from "./open-tasks-summary/open-tasks-summary.component";
import {CompletedTasksChartComponent} from "./completed-tasks-chart/completed-tasks-chart.component";
import {BugsComponent} from "./bugs/bugs.component";
import {ServerComponent} from "./server/server.component";
import {WebsiteComponent} from "./website/website.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

const COMPONENTS = [
  TasksComponent,
  FixedTasksSummaryComponent,
  OpenTasksSummaryComponent,
  CompletedTasksChartComponent,
  BugsComponent,
  ServerComponent,
  WebsiteComponent,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class TasksModule {
}
