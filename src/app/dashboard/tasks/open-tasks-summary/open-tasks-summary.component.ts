import {Component} from "@angular/core";
import {Task} from "../../../http/tasks/task.model";

@Component({
  selector: "app-open-tasks-summary",
  templateUrl: "./open-tasks-summary.component.html",
  styleUrls: ["./open-tasks-summary.component.scss"],
})
export class OpenTasksSummaryComponent {
  public tasks: Task[] = [];
  public openTaskCount: number;

  public calculate(): void {
    this.openTaskCount = this.tasks.filter((task) => !task.isCompleted()).length;
  }
}
