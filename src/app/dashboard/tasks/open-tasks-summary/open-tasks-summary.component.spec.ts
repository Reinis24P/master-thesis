import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {OpenTasksSummaryComponent} from "./open-tasks-summary.component";

describe("OpenTasksSummaryComponent", () => {
  let component: OpenTasksSummaryComponent;
  let fixture: ComponentFixture<OpenTasksSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OpenTasksSummaryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenTasksSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
