import {Task, TaskType} from "../../http/tasks/task.model";
import {TaskHttpService} from "../../http/tasks/task-http.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Injectable} from "@angular/core";

@Injectable()
export abstract class BaseTask {
  public abstract type: TaskType;
  public form: FormGroup;

  constructor(private taskHttpService: TaskHttpService, private fb: FormBuilder) {
    this.form = this.fb.group({
      name: new FormControl(null, Validators.required)
    });
  }

  public abstract filterTask(task: Task): boolean;

  public tasks(): Task[] {
    return [];
  }

  public createTask(): void {
  }

  public delete(task: Task): void {
  }

  public changeState(task: Task): void {
  }
}
