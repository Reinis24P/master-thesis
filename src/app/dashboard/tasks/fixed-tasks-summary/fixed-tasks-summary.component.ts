import {Component} from "@angular/core";
import {Task} from "../../../http/tasks/task.model";

@Component({
  selector: "app-fixed-tasks-summary",
  templateUrl: "./fixed-tasks-summary.component.html",
  styleUrls: ["./fixed-tasks-summary.component.scss"],
})
export class FixedTasksSummaryComponent {
  public tasks: Task[] = [];

  public fixedTaskCount: number;

  public calculate(): void {
    this.fixedTaskCount = this.tasks.filter((task) => task.isCompleted()).length;
  }
}
