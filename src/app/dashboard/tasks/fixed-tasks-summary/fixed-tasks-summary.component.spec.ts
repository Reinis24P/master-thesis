import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {FixedTasksSummaryComponent} from "./fixed-tasks-summary.component";

describe("FixedTasksSummaryComponent", () => {
  let component: FixedTasksSummaryComponent;
  let fixture: ComponentFixture<FixedTasksSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FixedTasksSummaryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedTasksSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
