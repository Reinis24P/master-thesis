import {Component} from "@angular/core";
import {Task, TaskType} from "../../../http/tasks/task.model";
import {BaseTask} from "../base-tasks";

@Component({
  selector: "app-server",
  templateUrl: "./server.component.html",
  styleUrls: ["./server.component.scss"],
})
export class ServerComponent extends BaseTask {
  public type: TaskType = "server";

  public filterTask(task: Task): boolean {
    return task.isServerType();
  }
}
