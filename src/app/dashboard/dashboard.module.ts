import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DashboardComponent} from "./dashboard.component";
import {TasksModule} from "./tasks/tasks.module";
import {MessagesModule} from "./messages/messages.module";
import {DiskModule} from "./disk/disk.module";

@NgModule({
  imports: [
    CommonModule,
    TasksModule,
    MessagesModule,
    DiskModule,
  ],
  declarations: [DashboardComponent],
  exports: [TasksModule, MessagesModule, DiskModule]
})
export class DashboardModule {
}
