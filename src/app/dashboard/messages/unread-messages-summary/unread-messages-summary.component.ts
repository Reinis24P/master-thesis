import {Component} from "@angular/core";
import {Message} from "../../../http/messages/message.model";

@Component({
  selector: "app-unread-messages-summary",
  templateUrl: "./unread-messages-summary.component.html",
  styleUrls: ["./unread-messages-summary.component.scss"],
})
export class UnreadMessagesSummaryComponent {
  public messages: Message[];
  public unreadMessagesCount: number;

  public calculate(): void {
    this.unreadMessagesCount = this.messages.filter((task) => !task.read).length;
  }
}
