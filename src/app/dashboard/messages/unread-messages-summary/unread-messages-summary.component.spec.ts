import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {UnreadMessagesSummaryComponent} from "./unread-messages-summary.component";

describe("UnreadMessagesSummaryComponent", () => {
  let component: UnreadMessagesSummaryComponent;
  let fixture: ComponentFixture<UnreadMessagesSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UnreadMessagesSummaryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnreadMessagesSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
