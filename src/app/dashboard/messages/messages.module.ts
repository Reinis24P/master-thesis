import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {UnreadMessagesSummaryComponent} from "./unread-messages-summary/unread-messages-summary.component";
import {MessagesComponent} from "./messages.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [UnreadMessagesSummaryComponent, MessagesComponent],
  exports: [UnreadMessagesSummaryComponent, MessagesComponent]
})
export class MessagesModule {
}
