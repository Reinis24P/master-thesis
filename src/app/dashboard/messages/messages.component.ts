import {Component} from "@angular/core";
import {Message} from "../../http/messages/message.model";

@Component({
  selector: "app-messages",
  templateUrl: "./messages.component.html",
  styleUrls: ["./messages.component.scss"],
})
export class MessagesComponent {
  public messages: Message[];

  public markAsRead(message: Message): void {
    if (message.read) {
      return;
    }

    const data = {
      ...message,
      read: true
    };
  }
}
