import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DiskUsageSummaryComponent} from "./disk-usage-summary/disk-usage-summary.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DiskUsageSummaryComponent],
  exports: [DiskUsageSummaryComponent]
})
export class DiskModule {
}
