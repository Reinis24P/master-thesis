import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {DiskUsageSummaryComponent} from "./disk-usage-summary.component";

describe("DiskUsageSummaryComponent", () => {
  let component: DiskUsageSummaryComponent;
  let fixture: ComponentFixture<DiskUsageSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DiskUsageSummaryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiskUsageSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
