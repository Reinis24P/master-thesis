import {Component} from "@angular/core";
import {Task} from "../../../http/tasks/task.model";
import {Message} from "../../../http/messages/message.model";

@Component({
  selector: "app-disk-usage-summary",
  templateUrl: "./disk-usage-summary.component.html",
  styleUrls: ["./disk-usage-summary.component.scss"],
})
export class DiskUsageSummaryComponent {
  public tasks: Task[] = [];
  public messages: Message[];
  public usedSpace: string;

  public calculate(): void {
    const reducer = (a, b) => a + b;
    const mapper = (a) => a.size;

    const tasks = (this.tasks || []).map(mapper).reduce(reducer, 0);
    const messages = (this.messages || []).map(mapper).reduce(reducer, 0);

    this.usedSpace = ((messages + tasks) / 1024 / 1024).toFixed(0);
  }
}
