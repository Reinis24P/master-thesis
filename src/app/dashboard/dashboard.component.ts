import {Component} from "@angular/core";
import {Task} from "../http/tasks/task.model";
import {Message} from "../http/messages/message.model";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent {
  public tasks: Task[] = [];
  public messages: Message[] = [];
}
