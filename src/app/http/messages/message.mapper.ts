import {BaseMapper} from "../base-mapper";
import {MessageHttp} from "./message-http.service";
import {Message} from "./message.model";
import {MessageFromMapper} from "./message-from.mapper";

export class MessageMapper implements BaseMapper<MessageHttp, Message> {
  public mapResponse(source: MessageHttp): Message {
    const response = new Message();
    response.id = source.id;
    response.from = new MessageFromMapper().mapResponse(source.from);
    response.received = new Date(source.received);
    response.message = source.message;
    response.read = source.read;
    response.size = source.size;

    return response;
  }

  public mapListResponse(source: MessageHttp[]): Message[] {
    return source.map((item) => this.mapResponse(item));
  }

  public map(source: Message): MessageHttp {
    return {
      id: source.id,
      from: new MessageFromMapper().map(source.from),
      received: source.received.toISOString(),
      message: source.message,
      read: source.read,
      size: source.size,
    };
  }

  public mapList(source: Message[]): MessageHttp[] {
    return source.map((item) => this.map(item));
  }
}
