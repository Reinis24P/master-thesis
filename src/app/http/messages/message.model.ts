import {MessageFromHttp} from "./message-http.service";

export class Message {
  public id: number;
  public from: MessageFromHttp;
  public received: Date;
  public message: string;
  public read: boolean;
  public size: number;
}

export class MessageFrom {
  public name: string;
  public username: string;
  public email: string;
}
