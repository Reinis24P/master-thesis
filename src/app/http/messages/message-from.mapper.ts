import {BaseMapper} from "../base-mapper";
import {MessageFromHttp} from "./message-http.service";
import {MessageFrom} from "./message.model";

export class MessageFromMapper implements BaseMapper<MessageFromHttp, MessageFrom> {
  public mapResponse(source: MessageFromHttp): MessageFrom {
    const response = new MessageFrom();
    response.name = source.name;
    response.username = source.username;
    response.email = source.email;

    return response;
  }

  public mapListResponse(source: MessageFromHttp[]): MessageFrom[] {
    return source.map((item) => this.mapResponse(item));
  }

  public map(source: MessageFrom): MessageFromHttp {
    return {
      name: source.name,
      username: source.username,
      email: source.email,
    };
  }

  public mapList(source: MessageFrom[]): MessageFromHttp[] {
    return source.map((item) => this.map(item));
  }
}
