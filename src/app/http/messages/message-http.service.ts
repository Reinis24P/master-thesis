import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {BaseHttpService} from "../base-http.service";
import {MessageMapper} from "./message.mapper";
import {Message} from "./message.model";

@Injectable()
export class MessageHttpService {
  constructor(private httpClient: HttpClient) {
  }

  public getMessages(): Observable<Message[]> {
    return this.httpClient
      .get<MessageHttp[]>(`${BaseHttpService.BASE_URL}/messages`)
      .map((messages) => new MessageMapper().mapListResponse(messages));
  }

  public updateMessage(message: Message): Observable<Message> {
    return this.httpClient
      .put<MessageHttp>(`${BaseHttpService.BASE_URL}/messages/${message.id}`, new MessageMapper().map(message))
      .map((message) => new MessageMapper().mapResponse(message));
  }
}

export interface MessageHttp {
  id: number;
  from: MessageFromHttp;
  received: string;
  message: string;
  read: boolean;
  size: number;
}

export interface MessageFromHttp {
  name: string;
  username: string;
  email: string;
}
