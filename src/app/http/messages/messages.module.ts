import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MessageHttpService} from "./message-http.service";

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [MessageHttpService]
})
export class MessagesModule {
}
