import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MessagesModule} from "./messages/messages.module";
import {TasksModule} from "./tasks/tasks.module";
import {UserModule} from "./users/user.module";
import {BaseHttpService} from "./base-http.service";

@NgModule({
  imports: [
    CommonModule,
    MessagesModule,
    TasksModule,
    UserModule
  ],
  providers: [BaseHttpService]
})
export class HttpModule {
}
