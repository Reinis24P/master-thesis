import {BaseMapper} from "../base-mapper";
import {UserHttp} from "./user-http.service";
import {User} from "./user.model";

export class UserMapper implements BaseMapper<UserHttp, User> {
  public mapResponse(source: UserHttp): User {
    const response = new User();
    response.name = source.name;
    response.username = source.username;
    response.email = source.email;
    response.phone = source.phone;
    response.website = source.website;
    response.roles = source.roles;
    response.password = source.password;

    return response;
  }

  public mapListResponse(source: UserHttp[]): User[] {
    return source.map((item) => this.mapResponse(item));
  }

  public map(source: User): UserHttp {
    return {
      name: source.name,
      username: source.username,
      email: source.email,
      phone: source.phone,
      website: source.website,
      roles: source.roles,
      password: source.password,
    };
  }

  public mapList(source: User[]): UserHttp[] {
    return source.map((item) => this.map(item));
  }
}
