export class User {
  public name: string;
  public username: string;
  public email: string;
  public phone: string;
  public website: string;
  public roles: ["admin" | "client" | "user"];
  public password: string;

  public isAdmin(): boolean {
    return this.roles.includes("admin");
  }
}
