import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {UserHttpService} from "./user-http.service";

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [UserHttpService]
})
export class UserModule {
}
