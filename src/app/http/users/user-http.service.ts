import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class UserHttpService {
  constructor(private httpClient: HttpClient) {
  }
}

export interface UserHttp {
  name: string,
  username: string,
  email: string,
  phone: string,
  website: string,
  roles: ["admin" | "client" | "user"],
  password: string,
}
