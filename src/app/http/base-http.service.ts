import {Injectable} from "@angular/core";

@Injectable()
export class BaseHttpService {
  public static readonly BASE_URL: string = "http://127.0.0.1:3000";
}
