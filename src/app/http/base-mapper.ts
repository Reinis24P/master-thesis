export interface BaseMapper<S, D> {
  mapResponse(source: S): D;

  mapListResponse(source: S[]): D[];

  map(source: D): S;

  mapList(source: D[]): S[];
}
