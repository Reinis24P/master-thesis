import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {TaskHttpService} from "./task-http.service";

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [TaskHttpService]
})
export class TasksModule {
}
