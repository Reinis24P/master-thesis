export class Task {
  public id: number;
  public assignee: string;
  public email: string;
  public createdAt: Date;
  public name: string;
  type: TaskType;
  status: TaskStatus;
  public completedAt?: Date;
  public size: number;

  public isCompleted(): boolean {
    return this.status === "done";
  }

  public isBugType(): boolean {
    return this.type === "bugs";
  }

  public isWebsiteType(): boolean {
    return this.type === "website";
  }

  public isServerType(): boolean {
    return this.type === "server";
  }
}

export type TaskType = "bugs" | "website" | "server";
export type TaskStatus = "toDo" | "inProgress" | "testing" | "done";
