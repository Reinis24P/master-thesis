import {BaseMapper} from "../base-mapper";
import {TaskHttp} from "./task-http.service";
import {Task} from "./task.model";

export class TaskMapper implements BaseMapper<TaskHttp, Task> {
  public mapResponse(source: TaskHttp): Task {
    const response = new Task();
    response.id = source.id;
    response.assignee = source.assignee;
    response.email = source.email;
    response.createdAt = new Date(source.createdAt);
    response.name = source.name;
    response.type = source.type;
    response.status = source.status;
    response.completedAt = source.completedAt ? new Date(source.completedAt) : undefined;
    response.size = source.size;

    return response;
  }

  public mapListResponse(source: TaskHttp[]): Task[] {
    return source.map((item) => this.mapResponse(item));
  }

  public map(source: Task): TaskHttp {
    return {
      id: source.id,
      assignee: source.assignee,
      email: source.email,
      createdAt: source.createdAt ? source.createdAt.toISOString() : undefined,
      name: source.name,
      type: source.type,
      status: source.status,
      completedAt: source.completedAt ? source.completedAt.toISOString() : undefined,
      size: source.size,
    };
  }

  public mapList(source: Task[]): TaskHttp[] {
    return source.map((item) => this.map(item));
  }
}
