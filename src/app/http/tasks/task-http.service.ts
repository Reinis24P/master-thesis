import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BaseHttpService} from "../base-http.service";
import {TaskMapper} from "./task.mapper";
import {Observable} from "rxjs/Observable";
import {Task, TaskStatus, TaskType} from "./task.model";

@Injectable()
export class TaskHttpService {
  constructor(private httpClient: HttpClient) {
  }

  public getTasks(): Observable<Task[]> {
    return this.httpClient
      .get<TaskHttp[]>(`${BaseHttpService.BASE_URL}/tasks`)
      .map((tasks) => new TaskMapper().mapListResponse(tasks));
  }

  public deleteTask(task: Task): Observable<void> {
    return this.httpClient.delete<void>(`${BaseHttpService.BASE_URL}/tasks/${task.id}`);
  }

  public updateTask(task: Task): Observable<Task> {
    return this.httpClient.put<TaskHttp>(`${BaseHttpService.BASE_URL}/tasks/${task.id}`, new TaskMapper().map(task))
      .map((task) => new TaskMapper().mapResponse(task));
  }

  public createTask(task: Task): Observable<Task> {
    return this.httpClient.post<TaskHttp>(`${BaseHttpService.BASE_URL}/tasks`, new TaskMapper().map(task))
      .map((task) => new TaskMapper().mapResponse(task));
  }
}

export interface TaskHttp {
  id: number,
  assignee: string,
  email: string,
  createdAt: string,
  name: string,
  type: TaskType,
  status: TaskStatus,
  completedAt?: string,
  size: number
}
